package com.company;

import java.util.HashMap;

//Solve using OOP and not FP
public class DuplicateEncoder {
	public static String encode(String word) {
		word = word.toLowerCase();
		HashMap<Character, Integer> counter = getCount(word);
		char[] output = word.toCharArray();
		for (int i = 0; i < word.length(); i++) {
			if(counter.get(output[i]) == 2) {
				output[i] = ')';
			} else {
				output[i] = '(';
			}
		}
		return new String(output);
	}

	private static HashMap<Character, Integer> getCount(String word) {
		HashMap<Character,Integer> counter = new HashMap<>();
		for(Character letter: word.toCharArray()){
			if(counter.containsKey(letter)){
				counter.put(letter, 2);
			}else {
				counter.put(letter, 1);
			}
		}
		return counter;
	}
}




